<?php
function vitrales_register_meta_boxes($meta_boxes)
{
  $prefix = 'vitral_';

  $meta_boxes[] = array(
    'id'         => 'vitrales',
    'title'      => 'Vitrales',
    'post_types' => 'vitrales',
    'context'    => 'normal',
    'priority'   => 'high',
    'fields'     => array(
      array(
        'name'              => 'Thumbnail',
        'id'                => $prefix . 'thumb',
        'type'              => 'image_upload',
        'max_status'        => false,
        'max_file_uploads'  => 1,
        'image_size'        => 'full'
      )
    )
  );

  return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'vitrales_register_meta_boxes' );
<?php
// Register Custom Post Type
function vitrales()
{
  $labels = array(
    'name'                => _x( 'Vitrales', 'Post Type General Name', 'vitrales' ),
    'singular_name'       => _x( 'Vitral', 'Post Type Singular Name', 'vitrales' ),
    'menu_name'           => __( 'Vitrales', 'vitrales' ),
    'name_admin_bar'      => __( 'Vitrales', 'vitrales' ),
    'parent_item_colon'   => __( 'Parent Item:', 'vitrales' ),
    'all_items'           => __( 'Todos Los Vitrales', 'vitrales' ),
    'add_new_item'        => __( 'Añadir Nuevo Vitral', 'vitrales' ),
    'add_new'             => __( 'Añadir Nuevo', 'vitrales' ),
    'new_item'            => __( 'Nuevo Vitral', 'vitrales' ),
    'edit_item'           => __( 'Editar Vitral', 'vitrales' ),
    'update_item'         => __( 'Actualizar Vitral', 'vitrales' ),
    'view_item'           => __( 'Ver Vitral', 'vitrales' ),
    'search_items'        => __( 'Buscar Vitral', 'vitrales' ),
    'not_found'           => __( 'No encontrado', 'vitrales' ),
    'not_found_in_trash'  => __( 'No encontrado en papelera', 'vitrales' ),
  );

  $args = array(
    'label'                 => __( 'Vitrales', 'vitrales' ),
    'description'           => __( 'Vitrales Custom Post Type', 'vitrales' ),
    'labels'                => $labels,
    'supports'              => array( 'author', 'revisions', 'title', 'thumbnail' ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-gallery',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => false,
    'capability_type'       => 'post',
    'show_in_rest'          => true,
    'rest_base'             => 'slider-api',
    'rest_controller_class' => 'WP_REST_Posts_Controller'
  );
  register_post_type( 'vitrales', $args );
}
add_action( 'init', 'vitrales', 0 );
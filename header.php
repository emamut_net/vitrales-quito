<!DOCTYPE html>
  <html lang="<?php language_attributes(); ?>" class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo( 'name' ); ?></title>

    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-63652125-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-63652125-1');
    </script>
  </head>
  <body data-spy="scroll" data-target=".navbar-fixed-top" id="page-top">
    <nav class="navbar navbar-custom navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle" data-target=".navbar-main-collapse" data-toggle="collapse" type="button">
            <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand page-scroll" href="#page-top">
            <i class="fa fa-picture-o"></i>
            <span class="light">Vitrales</span> Quito - Ecuador
          </a>
        </div>

        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">

            <li class="hidden">
              <a href="#page-top"></a>
            </li>

            <li>
              <a class="page-scroll" href="#gallery">Galería</a>
            </li>

            <li>
              <a class="page-scroll" href="#contact">Cont&aacute;ctenos</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

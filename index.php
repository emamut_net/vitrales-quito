<?php get_header();

  $vitrales_array = new WP_Query(array(
    'post_type' => 'vitrales' ,
    'order' => 'ASC',
    'posts_per_page' => -1
  ));
  $vitrales_array = $vitrales_array->posts;

  foreach ($vitrales_array as $post)
  {
    $custom_fields = get_post_custom($post->ID);

    $post->image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    $image = wp_get_attachment_image_src( $custom_fields['vitral_thumb'][0], 'full' );
    $post->thumb = $image[0];
  } ?>

    <header class="intro">
      <div class="intro-body">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <p class="intro-text">
                Con 12 años de experiencia en la fabricación de vitrales para residencias, locales comerciales,  clínicas, iglesias, etc.<br>
                Dispongo de cientos de diseños o le fabrico uno ideado por usted. <br>
                Se trabaja en las siguientes técnicas: <a href="#">Tiffany</a>, <a href="#">Cañuela de cobre</a>, <a href="#">Cañuela de plomo</a>, <a href="#">Vitromosaica</a> y <a href="#">Vitral en frio</a>.</strong>
              </p>

              <a class="btn btn-circle page-scroll" href="#gallery"><i class="fa fa-angle-double-down animated"></i></a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="container gallery-section text-center" id="gallery" ng-controller="galeria">
      <h2>Galería</h2>
      <div class="container">
        <div class="row">
          <?php foreach ($vitrales_array as $key => $vitral): ?>
            <div class="col-xs-12 col-md-3">
              <div class="thumbnail" data-src="<?php echo $vitral->image[0] ?>" data-caption="<?php echo $vitral->post_title ?>" data-toggle="modal" data-target="#myModal">
                <img class="img-responsive" src="<?php echo $vitral->thumb ?>" title="Haga click para ampliar">
                <div class="caption">
                  <h6><?php echo $vitral->post_title ?></h6>
                </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </section>

    <section class="container content-section text-center" id="contact">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <h2>Cont&aacute;ctenos</h2>

          <p>Uno de los encantos del vitral es convertir cualquier imagen en un cuadro en el que atraviesa la luz dándole el toque artístico a cualquier ambiente. Mayores informes a mis contactos:</p>
          <h3 class="brand-heading">Wilson Ramirez <br> <small style="color: #1D9B6C;">Vitralista</small></h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="list-inline banner-social-buttons">
            <li>
              <a class="btn btn-default btn-lg" href="#"><i class="fa fa-mobile fa-fw"></i> <span class="network-name">099 33 0 22 88</span></a>
            </li>

            <li>
              <a class="btn btn-default btn-lg" href="#"><i class="fa fa-phone fa-fw"></i> <span class="network-name">(02) 511 83 83</span></a>
            </li>

            <li>
              <a class="btn btn-default btn-lg" href="#"><i class="fa fa-home fa-fw"></i> <span class="network-name">El Carmen S7-309 y Alaquez</span></a>
            </li>

            <li>
              <a class="btn btn-default btn-lg" href="mailto:wilsonramirez4444@yahoo.es"><i class="fa fa-envelope-o"></i> wilsonramirez4444@yahoo.es</a>
            </li>
          </ul>

          <div id="map"></div>
        </div>
      </div>
    </section>

    <footer>
      <div class="container text-center">
        <p>Powered with <i class="fa fa-heart"></i> by <a href="https://emamut.net" target="_blank">emamut</a></p>
      </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <img class="img-responsive" src="" alt="">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>


<?php get_footer() ?>
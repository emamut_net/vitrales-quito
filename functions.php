<?php
require_once dirname( __FILE__ ) . '/helpers/TGM-Plugin-Activation-2.6.1/class-tgm-plugin-activation.php';

require_once dirname( __FILE__ ) . '/helpers/required-plugins.php';

require_once dirname( __FILE__ ) . '/helpers/vitrales-cpt.php';
require_once dirname( __FILE__ ) . '/helpers/vitrales-metabox.php';

add_theme_support( 'post-thumbnails' );

function emamut_setup()
{
  load_theme_textdomain( 'emamut' );
}
add_action( 'after_setup_theme', 'emamut_setup' );

function add_theme_scripts()
{
  wp_enqueue_style('Lora', 'https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic', array(), '1.1', 'all');
  wp_enqueue_style('Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,700', array(), '1.1', 'all');

  wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');

  wp_enqueue_script('main.min.js', get_template_directory_uri() . '/assets/js/main.min.js', array (), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );